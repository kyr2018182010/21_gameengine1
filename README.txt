조작키
이동 : w/a/s/d
점프 : space
회전 : 마우스
스킬 : 우클릭(조준) + 좌클릭(사용)
인벤토리 : I , 마우스 클릭
스킵 : F1(Prev), F2(Next) -> 플레이어 position만 스킵

Quest & Dialogue Graph
1. "The Legend Of JEONGYOON - Breath Of Mine" 프로젝트를 Unity Editor로 실행 
2. 상단 메뉴중 "Graph" 클릭해 Quest Graph / Narrative Graph 실행

- "../Asset/Resources" 폴더에 존재하는 Quest / Narrative Graph의 이름으로 기존에 존재하는 데이터 로드