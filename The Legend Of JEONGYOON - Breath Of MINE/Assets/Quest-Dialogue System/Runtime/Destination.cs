using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Subtegral.DialogueSystem.Runtime
{
    public class Destination : MonoBehaviour
    {
        public string guid;
        private Rigidbody _rigidbody;

        private void Start()
        {
            this.gameObject.SetActive(false);
            if (!TryGetComponent<Rigidbody>(out var rigidbody))
            {
                _rigidbody = gameObject.AddComponent<Rigidbody>();
                _rigidbody.useGravity = false;
                _rigidbody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
            }
        }

        private void OnDestroy()
        {
            if (_rigidbody)
                Destroy(_rigidbody);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "DestinationTarget")
            {
                this.gameObject.SetActive(false);
                Camera.main.GetComponent<QuestParser>().CheckArrived(guid);

            }
        }
    }
}


