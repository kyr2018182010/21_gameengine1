﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Subtegral.DialogueSystem.DataContainers;

namespace Subtegral.DialogueSystem.Runtime
{
    public class DialogueParser : MonoBehaviour
    {
        [SerializeField] public DialogueContainer dialogue;
        [SerializeField] private TextMeshProUGUI dialogueText;
        [SerializeField] private Button choicePrefab;
        [SerializeField] private Transform buttonContainer;
        public PlayerAction player;

        // other player's 
        [SerializeField] private TextMeshProUGUI dialogueText2;
        [SerializeField] private Transform buttonContainer2;

        private string questGuid = null;
        private QuestParser questParser;

        //public List<PlayerAction> PlayerAction { get => playerAction; set => playerAction = value; }

        private void Start()
        {
            questParser = this.transform.GetComponent<QuestParser>();
        }

        //public override void OnEnable()
        //{
        //    // 네트워크 이벤트 추가
        //    PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
        //}

        //public override void OnDisable()
        //{
        //    PhotonNetwork.NetworkingClient.EventReceived += EventReceived;
        //}


        public void StartTalk(string questGuid, bool isReceivePacket)
        {
            if (isReceivePacket)
            {
                dialogueText2.transform.parent.gameObject.SetActive(true);
            }
            else
            {
                dialogueText.transform.parent.gameObject.SetActive(true);
            }

            this.questGuid = questGuid;
            var narrativeData = dialogue.NodeLinks.First(); //Entrypoint node

            if (!isReceivePacket)
            {
                ProceedToNarrative(narrativeData.TargetNodeGUID);
            }
            else
            {
                StartOtherPlayerProceedToNarrative(narrativeData.TargetNodeGUID);
            }
        }

        private void ProceedToNarrative(string narrativeDataGUID)
        {
            var text = dialogue.DialogueNodeData.Find(x => x.NodeGUID == narrativeDataGUID).DialogueText;
            var choices = dialogue.NodeLinks.Where(x => x.BaseNodeGUID == narrativeDataGUID);
            dialogueText.text = ProcessProperties(text);
            var buttons = buttonContainer.GetComponentsInChildren<Button>();
            for (int i = 0; i < buttons.Length; i++)
            {
                Destroy(buttons[i].gameObject);
            }

            // 대화가 끝날 때
            if (choices.Count() == 0)
            {
                var choice = choices.ToList();
                var button = Instantiate(choicePrefab, buttonContainer);
                button.onClick.AddListener(() => EndDialogue(false));

                if(questGuid != "talk default")
                {
                    object[] content = new object[] { true };
                   // button.onClick.AddListener(() => SendRaiseEvent(EVENTCODE.END_DIALOGUE, content, SEND_OPTION.OTHER));
                }
                return;
            }

            // chioce가 1개 == 다음 버튼
            if (choices.Count() == 1)
            {
                var choice = choices.ToList();
                var button = Instantiate(choicePrefab, buttonContainer);
                button.GetComponentInChildren<Text>().text = ProcessProperties(choice[0].PortName);
                button.onClick.AddListener(() => ProceedToNarrative(choice[0].TargetNodeGUID));

                if (questGuid != "talk default")
                {
                    object[] content = new object[] { choice[0].TargetNodeGUID };
                    //button.onClick.AddListener(() => SendRaiseEvent(EVENTCODE.NEXT_DIALOGUE, content, SEND_OPTION.OTHER));
                }
                return;
            }

            // chioce가 2개 이상 == 선택지가 있을 때
            int cnt = 0;
            foreach (var choice in choices)
            {
                var button = Instantiate(choicePrefab, buttonContainer);
                button.name = button.name + cnt;
                button.GetComponentInChildren<Text>().text = ProcessProperties(choice.PortName);
                button.onClick.AddListener(() => ProceedToNarrative(choice.TargetNodeGUID));

                if (questGuid != "talk default")
                {
                    object[] content = new object[] { choice.TargetNodeGUID };
                   // button.onClick.AddListener(() => SendRaiseEvent(EVENTCODE.NEXT_DIALOGUE, content, SEND_OPTION.OTHER));
                }
                ++cnt;
                Debug.Log(ProcessProperties(choice.PortName));
            }
        }

        private void StartOtherPlayerProceedToNarrative(string narrativeDataGUID)
        {
            var text = dialogue.DialogueNodeData.Find(x => x.NodeGUID == narrativeDataGUID).DialogueText;
            var choices = dialogue.NodeLinks.Where(x => x.BaseNodeGUID == narrativeDataGUID);
            dialogueText2.text = ProcessProperties(text);
            var buttons = buttonContainer2.GetComponentsInChildren<Button>();
            for (int i = 0; i < buttons.Length; i++)
            {
                Destroy(buttons[i].gameObject);
            }

            // 대화가 끝날 때
            if (choices.Count() == 0)
            {
                var choice = choices.ToList();
                var button = Instantiate(choicePrefab, buttonContainer2);
                //button.onClick.AddListener(() => EndDialogue());
                return;
            }

            // chioce가 1개 == 다음 버튼
            if (choices.Count() == 1)
            {
                var choice = choices.ToList();
                var button = Instantiate(choicePrefab, buttonContainer2);
                button.GetComponentInChildren<Text>().text = ProcessProperties(choice[0].PortName);
                //button.onClick.AddListener(() => StartOtherPlayerProceedToNarrative(choice[0].TargetNodeGUID));
                return;
            }

            int cnt = 0;
            // chioce가 2개 이상 == 선택지가 있을 때
            foreach (var choice in choices)
            {
                var button = Instantiate(choicePrefab, buttonContainer2);
                button.name = button.name + cnt;
                button.GetComponentInChildren<Text>().text = ProcessProperties(choice.PortName);
                ++cnt;
                //button.onClick.AddListener(() => StartOtherPlayerProceedToNarrative(choice.TargetNodeGUID));
            }
        }

        private string ProcessProperties(string text)
        {
            foreach (var exposedProperty in dialogue.ExposedProperties)
            {
                text = text.Replace($"[{exposedProperty.PropertyName}]", exposedProperty.PropertyValue);
            }
            return text;
        }

        private void EndDialogue(bool isReceivePacket)
        {
            if(!isReceivePacket)
                dialogueText.transform.parent.gameObject.SetActive(false);
            else
                dialogueText2.transform.parent.gameObject.SetActive(false);

            // 퀘스트 수령 다이얼로그면
            if (questGuid != "not quest give" && questGuid != "talk default")
            {
                if(!isReceivePacket)
                    questParser.ProceedToNarrative(questGuid);
                else
                    questParser.OtherPlayerProceedToNarrative(questGuid);
                questGuid = null;
                return;
            }


            player.SetTalkState(false, null);
            questGuid = null;
        }

        //public void SendRaiseEvent(EVENTCODE eventcode, object[] content, SEND_OPTION sendoption)
        //{
        //    // debug
        //    string DebugStr = string.Empty;
        //    DebugStr = "[SEND__" + eventcode.ToString() + "]";
        //    for (int i = 0; i < content.Length; ++i)
        //    {
        //        DebugStr += "_" + content[i];
        //    }
        //    Debug.Log(DebugStr);

        //    RaiseEventOptions raiseEventOption = new RaiseEventOptions
        //    {
        //        Receivers = (ReceiverGroup)sendoption,
        //    };
        //    PhotonNetwork.RaiseEvent((byte)eventcode, content, raiseEventOption, SendOptions.SendReliable);
        //}

        //private void EventReceived(EventData photonEvent)
        //{
        //    int code = photonEvent.Code;

        //    if (code == (int)EVENTCODE.NEXT_DIALOGUE)
        //    {
        //        object[] datas = (object[])photonEvent.CustomData;
        //        Debug.Log("RECV__EVENTCODE.INSTANCIATE_ENEMY__" + datas[0]);

        //        StartOtherPlayerProceedToNarrative((string)datas[0]);
        //    }
        //    else if (code == (int)EVENTCODE.END_DIALOGUE)
        //    {
        //        object[] datas = (object[])photonEvent.CustomData;
        //        Debug.Log("RECV__EVENTCODE.INSTANCIATE_ENEMY__" + datas[0]);

        //        EndDialogue(true);
        //    }
        //}
    }
}