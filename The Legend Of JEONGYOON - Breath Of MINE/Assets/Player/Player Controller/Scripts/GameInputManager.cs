using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Inputdata
{
    public Vector2 runData;
    public bool rightMouseHold;
    public Vector2 CameraData;
}

public class GameInputManager : MonoBehaviour
{
    public static Inputdata inputData;

    private PlayerAction player;

    //private FollowQuestPlayer questPlayer;

    public SkillManager skillManager;


    public NPCSelector npcSelector;
    public NPCSelector NPCSelector
    {
        set => npcSelector = value;
    }


    private void Start()
    {
        player = GetComponent<PlayerAction>();
        if (player == null)
            Debug.LogError("GameInputManager : No Player");
        //else
        //{
        //    questPlayer = player.GetComponent<FollowQuestPlayer>();
        //}

        if (skillManager == null)
            Debug.LogError("Game Input Manager : No Skill Manager");

        if (npcSelector == null)
            Debug.LogError("Game Input Manager : No NPC Selector");

    }
    private void Update()
    {
        if (GameManager.currState != GameState.MainGame) return;

        PlayerInput();

        CameraInput();

        SkillInput();

        LeftClickInput();
    }

    private void PlayerInput()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        inputData.runData.x = x;
        inputData.runData.y = y;

        if (Input.GetButtonDown("Jump"))
            player.JumpStart();
    }

    private void CameraInput()
    {
        float rotX = Input.GetAxis("Mouse X");
        float rotY = Input.GetAxis("Mouse Y");
        inputData.CameraData = new Vector2(rotX, rotY);
    }

    private void SkillInput()
    {
        if (skillManager == null)
            return;

        if (Input.GetButtonDown("Skill"))
        {
            skillManager.SkillButton(true);
            player.SetSkillAnimation(true);
        }

        if (Input.GetButtonUp("Skill"))
        {
            skillManager.SkillButton(false);
            player.SetSkillAnimation(false);
        }
    }

    private void LeftClickInput()
    {
        if (Input.GetButtonDown("LeftClick"))
        {
            var exeuted = skillManager.SelectButton();
            if (exeuted)
                player.SetSkillAnimation(false);
            else
                npcSelector.SelectionButton();
        }
    }
}
