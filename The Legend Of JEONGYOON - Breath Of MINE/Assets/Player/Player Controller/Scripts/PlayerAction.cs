﻿using UnityEngine;
using System.Collections;

public enum FootStepType
{
    WalkOnGrass = 0,
    WalkOnTile,
}

public class PlayerAction : MonoBehaviour
{
    private CharacterController _characterController;
    private Animator _animator;
    private Rigidbody rigidbody;
    private AudioSource audioSource;

    // for test
    [SerializeField] private PlayerState _state = PlayerState.Idle;
    public PlayerState State
    {
        get => _state;
        set => _state = value;
    }

    // animation
    private readonly int _hashJumpStart = Animator.StringToHash("JumpStart");
    private readonly int _hashJumping = Animator.StringToHash("Jumping");
    private readonly int _hashRunJump = Animator.StringToHash("Run Jump");
    private readonly int _hashEndRunJump = Animator.StringToHash("End Run Jump");
    private readonly int _hashLand = Animator.StringToHash("Land");
    private readonly int _hashFall = Animator.StringToHash("Fall");
    private readonly int _hashInput = Animator.StringToHash("Input");
    private readonly int _hashHor = Animator.StringToHash("InputHor");
    private readonly int _hashVer = Animator.StringToHash("InputVer");
    private readonly int _hashPush = Animator.StringToHash("Push");
    private readonly int _hashSkill = Animator.StringToHash("Skill");
    private readonly int _hashTalk = Animator.StringToHash("Talk");

    // Move
    [Header("Move")]
    [SerializeField] private float runSpeed = 5f;
    [SerializeField] private float walkSpeed = 3f;
    [SerializeField] private float sideWalkSpeed = 2f;
    private bool onTile;
    private Vector3 moveVelocity = Vector3.zero;

    //Jump
    [Header("Jump")]
    [SerializeField] private float jumpPower = 300f;
    [SerializeField] private float jumpDuration = 0.28f;
    [SerializeField] private float landMinDist = 1f;
    private float _jumpDuration;
    [SerializeField] private float moveSpeedDiminution = 0.1f;
    private float _moveSpeedDiminution = 1f;

    // Fall
    [Header("Fall")]
    [SerializeField] private float mass = 10f;
    [SerializeField] private float minGroundedDist = 0.02f;
    [SerializeField] private float fallDistance = 5f;
    private int layerMask;
    private Vector3 _fallVelocity = Vector3.zero;
    private float _fallingDistance;
    [SerializeField] private float deadFallingDistance;
    private float groundDist = 10f;

    [HideInInspector] public Vector3 externalVelocity = Vector3.zero;

    [SerializeField] private float maxWaterSeconds = 10f;
    private float restWaterSeconds;

    // Sound
    [SerializeField] AudioData[] audioClips;
    FootStepType currFootStepType = FootStepType.WalkOnGrass;


    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        layerMask = 1 << LayerMask.NameToLayer("Water") | 1 << LayerMask.NameToLayer("Player") | 1 <<LayerMask.NameToLayer("Water Area");
        layerMask = ~layerMask;

        // add rigidbody
        rigidbody = GetComponent<Rigidbody>();
        if (!rigidbody)
        {
            rigidbody = gameObject.AddComponent<Rigidbody>();
        }

        rigidbody.useGravity = false;
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        rigidbody.mass = 0.5f;

        audioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        // reset values
        _fallVelocity = Vector3.zero;
        externalVelocity = Vector3.zero;
        restWaterSeconds = maxWaterSeconds;
        _fallingDistance = 0f;
        onTile = false; 
        _state = PlayerState.Idle;
        _moveSpeedDiminution = 1f;
        _jumpDuration = jumpDuration;
        currFootStepType = FootStepType.WalkOnGrass;
        SetFootStepSound(currFootStepType);
    }

    private void FixedUpdate()
    {
        if (_state == PlayerState.Talk)
            return;

        moveVelocity = Vector3.zero;
        Fall();

        if (_state == PlayerState.Dead)
            return;

        Jumping();
        Move();
        CheckisGrounded();

        var vec = (moveVelocity + _fallVelocity + externalVelocity) * Time.deltaTime;
        _characterController.Move(vec);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            restWaterSeconds -= Time.deltaTime;
            if (restWaterSeconds <= 0)
            {
                restWaterSeconds = maxWaterSeconds;
                GameManager.GameOver();
            }
        }
    }

    private void Move()
    {
        // �÷��̾ x�� �������� ȸ����Ų��.

        if (_state != PlayerState.Jumping)
            transform.Rotate(Vector3.up * (GameInputManager.inputData.CameraData.x * Time.deltaTime * CameraMoving.MouseXSensitivity));

        Vector2 runData = GameInputManager.inputData.runData;
        Vector2 speed = Vector2.zero;

        if (_state != PlayerState.Jumping && _state != PlayerState.JumpStart)
        {
            if (runData == Vector2.zero)
            {
                if (_state == PlayerState.Run)
                    _state = PlayerState.Idle;
                if (audioSource.isPlaying)
                    audioSource.Stop();
            }
            else
            {
                if (_state == PlayerState.Fall)
                {
                    speed = new Vector2(runSpeed, runSpeed);
                }
                else
                {
                    if (runData.y > 0)
                    {
                        // forward run
                        speed = new Vector2(runSpeed, runSpeed);
                        if (!audioSource.isPlaying)
                        {
                            audioSource.Play();
                        }
                            audioSource.pitch = audioClips[(int)currFootStepType].pitch * 1.3f;

                    }
                    else if (runData.y < 0)
                    {
                        // back walk
                        speed = new Vector2(walkSpeed, walkSpeed);
                        if (!audioSource.isPlaying)
                        {
                            audioSource.Play();
                        }
                        audioSource.pitch = audioClips[(int)currFootStepType].pitch;

                    }
                    else if (runData.y == 0)
                    {
                        // side walk
                        if (!audioSource.isPlaying)
                        {
                            audioSource.Play();
                        }
                        audioSource.pitch = audioClips[(int)currFootStepType].pitch * 0.8f;
                        speed.x = sideWalkSpeed;
                        speed.y = 0f;
                    }
                    _state = PlayerState.Run;

                }
            }

            // set animation
            if (_state != PlayerState.Fall)
            {
                _animator.SetFloat(_hashInput, runData.magnitude);
                _animator.SetFloat(_hashHor, runData.x);
                _animator.SetFloat(_hashVer, runData.y);
            }
            else
            {
                _animator.SetFloat(_hashInput, 0f);
            }


        }
        else
            speed.y = runSpeed;

        runData = runData.normalized;
        moveVelocity = (transform.right * runData.x * speed.x + transform.forward * runData.y * speed.y) * _moveSpeedDiminution;

        if (_state != PlayerState.Run && audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }

    void CheckisGrounded()
    {
        Debug.DrawRay(transform.position + Vector3.up * 0.3f, -transform.up * fallDistance, Color.red);
        if (Physics.Raycast(transform.position + Vector3.up * 0.3f, -transform.up, out var hit, fallDistance, layerMask))
        {
            //hitPoint = hit.point;
            _fallingDistance = 0f;
            groundDist = transform.position.y - hit.point.y;

            // Set foot step sound
            if (hit.transform.CompareTag("Tile"))
            {
                SetFootStepSound(FootStepType.WalkOnTile);
            }
            else if(hit.transform.CompareTag("Floor"))
            {
                SetFootStepSound(FootStepType.WalkOnGrass);
            }

            // Land Finished Check
            if (_state != PlayerState.JumpStart && _fallVelocity.y <= 0)
            {
                if (groundDist <= 0.03f)
                {
                    moveVelocity += groundDist * Vector3.down;
                    _fallVelocity = Vector3.zero;
                }

                if (onTile && _state != PlayerState.Jumping && externalVelocity.y == 0 && groundDist > 0.03f)
                {
                    moveVelocity += groundDist * Vector3.down;
                    _fallVelocity = Vector3.zero;
                }

                if (_state == PlayerState.Jumping || _state == PlayerState.Fall)
                {
                    if (groundDist < landMinDist)
                    {
                        if (GameInputManager.inputData.runData == Vector2.zero)
                            _animator.SetTrigger(_hashLand);
                        else
                            _animator.SetTrigger(_hashEndRunJump);
                        _animator.SetBool(_hashFall, false);
                        _state = PlayerState.Landing;
                    }
                }

                if (_state == PlayerState.Landing && groundDist < minGroundedDist)
                {
                    _state = PlayerState.Idle;
                    _moveSpeedDiminution = 1f;
                }
            }
        }
        else
        {
            // Fall Check
            if (_state != PlayerState.Fall && _state != PlayerState.Jumping && _state != PlayerState.JumpStart)
            {
                _animator.SetBool(_hashFall, true);
                _state = PlayerState.Fall;
                _fallingDistance = 0f;
                _fallVelocity = Vector3.zero;
            }
        }
    }

    private void SetFootStepSound(FootStepType clipName)
    {
        if (currFootStepType != clipName) {
            audioSource.clip = audioClips[(int)clipName].clip;
            audioSource.volume = audioClips[(int)clipName].volume;
            currFootStepType = clipName;
        }
    }

    private void Fall()
    {
        if (!onTile || _state == PlayerState.Jumping || externalVelocity.y <= 0)
        {
            float accY = -9.8f;
            _fallVelocity.y += accY * Time.deltaTime;
        }

        // check State            
        _fallingDistance -= _fallVelocity.y * Time.deltaTime;

        // Dead Check
        if (_fallingDistance >= deadFallingDistance && _state != PlayerState.Dead)
            _state = PlayerState.Dead;


        if (Physics.Raycast(transform.position, transform.up, out var hit, _fallingDistance, layerMask))
        {
            // adjust player feet position
            groundDist = transform.position.y - hit.point.y;
            if (groundDist <= 0)
            {
                _characterController.Move(groundDist * Vector3.down);
                _fallVelocity = Vector3.zero;
                _fallingDistance = 0f;
            }
        }
    }

    public void JumpStart()
    {
        if (_state != PlayerState.Jumping && _state != PlayerState.JumpStart && _state != PlayerState.Landing)
        {
            if (_state == PlayerState.Run)
            {
                _jumpDuration = 0f;
            }
            else
                _animator.SetTrigger(_hashJumpStart);
            _state = PlayerState.JumpStart;
            _moveSpeedDiminution = moveSpeedDiminution;
        }
    }

    private void Jumping()
    {
        if (_state != PlayerState.JumpStart) return;
        if (_jumpDuration <= 0)
        {
            // Start Jump
            _animator.SetTrigger(_hashJumping);
            _fallVelocity = Vector3.zero;
            float acc = (jumpPower / mass) * 10;
            _fallVelocity.y += acc * Time.deltaTime;
            _state = PlayerState.Jumping;
            _jumpDuration = jumpDuration;
        }
        else
        {
            _jumpDuration -= Time.deltaTime;
        }
    }

    public void SetSkillAnimation(bool skill)
    {
        _animator.SetBool(_hashSkill, skill);
    }

    public void SetTalkState(bool talk, Transform npc)
    {
        if (talk)
        {
            _state = PlayerState.Talk;
            var pos = npc.position;
            pos.y = transform.position.y;
            transform.LookAt(pos, transform.up);
            _animator.SetFloat(_hashInput, 0);      // change animation to idle
            if (audioSource.isPlaying)
            {
                audioSource.Stop();
            }
        }
        else
        {
            _state = PlayerState.Idle;
        }

        _animator.SetFloat(_hashInput, 0f);
        _animator.SetBool(_hashTalk, talk);
        GameManager.ShowCursor(talk);
    }

    public void SetNewTransform(Vector3 pos)
    {
        _characterController.enabled = false;
        transform.position = pos;
        _characterController.enabled = true;
        GameManager.SetSpawnPoint(pos);
    }

    public void OnTile(bool isOnTile)
    {

        var originOnTile = onTile;          // prevent to start same coroutine twice
        onTile = isOnTile;
        if (isOnTile && !originOnTile)
        {
            externalVelocity = Vector3.zero;
            Debug.Log("start on Tile Update");
            StartCoroutine(OnTileUpdate());
        }
    }

    IEnumerator OnTileUpdate()
    {
        while (true)
        {
            if (!onTile)
            {
                StopCoroutine(OnTileUpdate());
                externalVelocity = Vector3.zero;
                break;
            }
            Debug.Log("On Tile");
            if (onTile && externalVelocity.y > 0)
            {
                rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX
                    | RigidbodyConstraints.FreezePositionZ;
            }
            else
                rigidbody.constraints = RigidbodyConstraints.FreezeAll;

            yield return new WaitForEndOfFrame();
        }
    }

}
