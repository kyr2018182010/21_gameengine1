using UnityEngine;
using UnityEngine.UI;

public class SkillManager : MonoBehaviour
{
    [SerializeField]private Text focus;
     
    [HideInInspector] public bool isKeyHolding;
    [HideInInspector] public Skill2Object _selection;

    [Header("Skill2")]
    private Vector3 _creationPoint;
    public Vector3 CreationPoint
    {
        set => _creationPoint = value;
    }
    private bool _creatable;
    public bool Creatable
    {
        set => _creatable = value;
    }

    [SerializeField] GameObject skill2ObjectPrefab;
    [SerializeField] Material skill2UIMateiral;
    [SerializeField] private Color defualtUIColor;
    private Skill2Object[] skill2Objects;
    private GameObject skill2UIObject;
    private const int maxObjectCount = 5;
    private const float maxDuration = 15f;
    private int objectCount = 0;

    private PlayerAction player;
    public PlayerAction Player
    {
        set => player = value;
    }

    public void Awake()
    {
        // set Focus UI
        if (focus == null)
            Debug.LogError("SKill Manager focus is NULL");

        // skill2의 보석 프리팹을 복사해 보석 프리팹 배열에 넣어준다.
        skill2Objects = new Skill2Object[maxObjectCount];
        for (int i = 0; i < maxObjectCount; ++i)
        {
            var obj = Instantiate(skill2ObjectPrefab) as GameObject;
            skill2Objects[i] = obj.GetComponent<Skill2Object>();
            skill2Objects[i].Id = i;
        }

        // skill2의 UI로 사용할 오브젝트를 프리팹을 복사해 생성한다.
        skill2UIObject = Instantiate(skill2ObjectPrefab) as GameObject;
        var renderer = skill2UIObject.GetComponentInChildren<Renderer>();
        if (renderer != null)
        {
            renderer.material = skill2UIMateiral;
        }
        skill2UIObject.GetComponent<Collider>().isTrigger = true;
       // skill2UIObject.GetComponent<Rigidbody>().isKinematic = true;
        var skill2Comp = skill2UIObject.GetComponent<Skill2Object>();
        Destroy(skill2Comp);
        skill2UIObject.layer = 2;           // set layer to ignore raycast
        skill2UIObject.AddComponent<Skill2UI>();
        skill2UIObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        if (isKeyHolding && _creatable && _selection == null)
        {
            // Skill2 UI
            if (objectCount >= maxObjectCount)
            {
                skill2UIMateiral.SetColor("_EmissionColor", Color.red);
            }
            else
            {
                skill2UIMateiral.SetColor("_EmissionColor", defualtUIColor);
            }

            if (!skill2UIObject.activeSelf)
                skill2UIObject.SetActive(true);
            skill2UIObject.transform.position = _creationPoint;
        }
        else if (!_creatable || !isKeyHolding || _selection != null)
        {
            if (skill2UIObject.activeSelf)
                skill2UIObject.SetActive(false);
        }

        // 지속시간이 지난 오브젝트를 검사해 삭제한다.
        CheckDurationOverObject();

        if (_selection != null && !isKeyHolding)
            _selection = null; if (isKeyHolding && _creatable && _selection == null)
        {
            if (objectCount >= maxObjectCount || !skill2UIObject.GetComponent<Skill2UI>().creatable)
            {
                skill2UIMateiral.SetColor("_EmissionColor", Color.red);
            }
            else
            {
                skill2UIMateiral.SetColor("_EmissionColor", defualtUIColor);
            }
            if (!skill2UIObject.activeSelf)
                skill2UIObject.SetActive(true);
            skill2UIObject.transform.position = _creationPoint;
        }
        else if (!_creatable || !isKeyHolding || _selection != null)
        {
            if (skill2UIObject.activeSelf)
                skill2UIObject.SetActive(false);
        }

        // 지속시간이 지난 오브젝트를 검사해 삭제한다.
        CheckDurationOverObject();

        if (_selection != null && !isKeyHolding)
            _selection = null;

    }

    int FindCreatableObject()
    {
        for (int i = 0; i < maxObjectCount; ++i)
        {
            if (!skill2Objects[i].gameObject.activeSelf)
            {
                Debug.Log("Find : " + i);
                return i;
            }
        }
        Debug.Log("Can't Find");
        return -1;
    }

    void CreateObject(int idx)
    {
        skill2Objects[idx].transform.position = _creationPoint;
        skill2Objects[idx].gameObject.SetActive(true);
        objectCount++;
    }

    void DeleteObject(Skill2Object selection)
    {
        var idx = selection.Id;
        skill2Objects[idx].gameObject.SetActive(false);
        objectCount--;
    }

    void CheckDurationOverObject()
    {
        foreach (var obj in skill2Objects)
        {
            if (obj.gameObject.activeSelf && obj.Duration >= maxDuration)
            {
                obj.gameObject.SetActive(false);
                objectCount--;
            }
        }
    }

    public void SkillButton(bool hold)
    {
        isKeyHolding = hold;
        focus.enabled = hold;
    }


    public bool SelectButton()
    {
        bool returnVal = false;

        if (_selection != null)
        {
            DeleteObject(_selection);
            returnVal = true;
        }
        else
        {
            if (_creatable && objectCount < maxObjectCount && skill2UIObject.GetComponent<Skill2UI>().creatable)
            {
                var idx = FindCreatableObject();
                if (idx >= 0)
                    CreateObject(idx);
                returnVal = true;
            }
        }

        isKeyHolding = false;
        focus.enabled = false;

        return returnVal;
    }
}
