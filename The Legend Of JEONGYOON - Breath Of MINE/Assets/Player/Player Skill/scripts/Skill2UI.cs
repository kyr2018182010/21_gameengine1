using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

public class Skill2UI : MonoBehaviour
{
    public bool creatable = true;
    private Collider collider;
    private Rigidbody rigidbody;

    private bool gravityHeavy = false;
    private bool staticObject = false;

    private void Start()
    {
        collider = GetComponent<Collider>();
        collider.isTrigger = true;
        if (rigidbody == null)
            rigidbody = gameObject.AddComponent<Rigidbody>();
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
    }

    private void OnEnable()
    {
        creatable = true;
        gravityHeavy = false;
        staticObject = false;
    }

    private void FixedUpdate()
    {
        // if collide with gravity Heavy Object or staticObject skillObject can't be created
        if (gravityHeavy || staticObject)
        {
            creatable = false;
        }
        else
        {
            creatable = true;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponentInParent<PlayerAction>();

        // if static Object
        if (player == null  && !other.CompareTag("Water"))
        {
            staticObject = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        var player = other.GetComponentInParent<PlayerAction>();

        // if static Object
        if (player == null /*&& npc == null*/ && !other.CompareTag("Water"))
        {
            staticObject = false;
        }
    }
}
