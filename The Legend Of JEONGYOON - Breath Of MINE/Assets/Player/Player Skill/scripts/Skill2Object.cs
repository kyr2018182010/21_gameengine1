using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill2Object : OutlineObject
{
    private float duration = 0f;
    public float Duration
    {
        get => duration;
    }
    private bool creating;
    public bool Creating
    {
        set => creating = value;
    }
    

    private const float defualtRestCreatedHeight = 4f;
    private float restCreatedHeight;
    private const float createdDetal = 2f;

    private int id;
    public int Id
    {
        get => id;
        set => id = value;
    }

    private void Start()
    {
        duration = 0f;
        creating = true;
        transform.Translate(Vector3.down * defualtRestCreatedHeight);
        restCreatedHeight = defualtRestCreatedHeight;

        var rigid = gameObject.AddComponent<Rigidbody>();
        rigid.constraints = RigidbodyConstraints.FreezeAll;
        rigid.useGravity = false;

        MakeOutlineModel();
    }

    private void OnEnable()
    {
        duration = 0f;
        creating = true;
        transform.Translate(Vector3.down * defualtRestCreatedHeight);
        restCreatedHeight = defualtRestCreatedHeight;
    }

    private void FixedUpdate()
    {
        duration += Time.deltaTime;
        if (creating)
            Created();
    }

    public void Created()
    {
        transform.Translate(Vector3.up * createdDetal * Time.deltaTime);
        restCreatedHeight -= createdDetal * Time.deltaTime;
        if (restCreatedHeight <= 0)
        {
            creating = false;
        }
    }
}
