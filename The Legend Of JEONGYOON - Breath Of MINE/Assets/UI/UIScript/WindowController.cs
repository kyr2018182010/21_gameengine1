using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowController : MonoBehaviour
{
    [SerializeField] private List<GameObject> panels;
    private Dictionary<string, GameObject> panelDicts = new Dictionary<string, GameObject>();

    private void Start()
    {
        foreach(var p in panels)
        {
            panelDicts.Add(p.name, p);
        }
    }
    public void OnClickOpenOrClose(string panelName) 
    {
        var panel = panelDicts[panelName];
        panel.SetActive(!panel.activeSelf);
    }
}
