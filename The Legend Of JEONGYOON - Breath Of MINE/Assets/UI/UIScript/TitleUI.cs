﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class uiGameObj : MonoBehaviour
{
    public GameObject obj;
    public IEnumerator fadeInCoroutine;
    Color fadeVal = new Color(0f, 0f, 0f, 0.01f);

    public uiGameObj(GameObject g)
    {
        obj = g;
        fadeInCoroutine = FadeIn(obj);
    }

    IEnumerator FadeIn(GameObject obj)
    {
        while (true)
        {
            obj.GetComponent<Image>().color += fadeVal;


            yield return new WaitForSeconds(0.01f);
        }
    }
}


public class TitleUI : MonoBehaviour
{
    [SerializeField]
    GameObject logo = null;
    [SerializeField]
    GameObject StartButton = null;
    [SerializeField]
    GameObject ExitButton = null;

    [SerializeField]
    float buttonY = 100;

    Vector3 scaleVal = new Vector3(0.001f, 0.001f, 0.001f);

    List<uiGameObj> UIs = new List<uiGameObj>();

    void Start()
    {
        UIs.Add(new uiGameObj(logo));
        UIs.Add(new uiGameObj(StartButton));
        UIs.Add(new uiGameObj(ExitButton));

        StartCoroutine(UIs[0].fadeInCoroutine);
        //Invoke("TitleMove", 2);
        SetActiveButton();


    }

    void Update()
    {
        for(int i =0;i< UIs.Count; ++i)
        {
            if (UIs[i].obj.GetComponent<Image>().color.a == 1.0f)
            {
                StopCoroutine(UIs[i].fadeInCoroutine);
                UIs.RemoveAt(i);
                --i;
            }
        }
    }

    void SetActiveButton()
    {
        StartButton.SetActive(true);
        ExitButton.SetActive(true);

        StartCoroutine(UIs[1].fadeInCoroutine);
        StartCoroutine(UIs[2].fadeInCoroutine);

        ButtonMove();
    }

    void ButtonMove()
    {
        Vector3 moveVal = StartButton.transform.position + new Vector3(0.0f, 30f, 0.0f);
        StartButton.transform.position = Vector3.Lerp(StartButton.transform.position, moveVal, 0.005f);

        moveVal = ExitButton.transform.position + new Vector3(0.0f, 30f, 0.0f);
        ExitButton.transform.position = Vector3.Lerp(ExitButton.transform.position, moveVal, 0.005f);

        if(!(StartButton.transform.position.y > buttonY))
            Invoke("ButtonMove", 0.01f);
    }

    void TitleMove()
    {
        logo.gameObject.transform.localScale -= scaleVal;

        Vector3 moveVal = logo.transform.position + new Vector3(0.0f, 50f, 0.0f);
        logo.transform.position = Vector3.Lerp(logo.transform.position, moveVal, 0.005f);

        if (!(logo.transform.lossyScale == new Vector3(0.8f, 0.8f, 0.8f)))
            Invoke("TitleMove", 0.01f);
        else
            SetActiveButton();
    }

    public void GameStart()
    {
        Debug.Log("start");
        SceneManager.LoadScene("SampleScene");
    }

    public void GameExit()
    {
        Debug.Log("exit");
    }
}