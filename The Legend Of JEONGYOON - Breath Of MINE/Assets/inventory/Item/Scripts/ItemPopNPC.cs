using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct CollectionData
{
    public string name;
    public int count;
}

public class ItemPopNPC : MonoBehaviour
{
    [HideInInspector]public bool QuestStart = false;
    [HideInInspector] public bool QuestEnd = false;
   [SerializeField] private CollectionData[] collectionData;

    public void RetrieveItems()
    {
        foreach(var item in collectionData)
        {
            Inventory.PopItem(item.name, item.count);
        }
    }
    
}
