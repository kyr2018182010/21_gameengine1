﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field)]
public class ItemPropertyNameAttribute :Attribute
{
    public string ItemName;
    public ItemPropertyNameAttribute(string itemName)
    {
        ItemName = itemName;
    }
}
