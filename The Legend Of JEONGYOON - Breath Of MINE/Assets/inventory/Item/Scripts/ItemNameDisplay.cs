﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemNameDisplay : MonoBehaviour
{
    public itemDatabase itemDatabase;
    private Text _text;

    private void Start()
    {
        _text = GetComponent<Text>();
        foreach(var item in itemDatabase.itemDatas)
        {
            _text.text += item.itemName + Environment.NewLine;
        }
    }
}
