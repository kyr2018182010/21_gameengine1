﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(order = 0, fileName ="아이템 정보",menuName = " KPU/아이템 정보 생성하기")]
public class ItemData : ScriptableObject
{
    public string itemName;
    public string explain;
    public int itemId;
    public Sprite sprite;
}
