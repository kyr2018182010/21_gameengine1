﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(order = 1, fileName = "아이템 Database", menuName = " KPU/아이템 Database 생성하기")]

public class itemDatabase : ScriptableObject
{
    public List<ItemData> itemDatas;
}
