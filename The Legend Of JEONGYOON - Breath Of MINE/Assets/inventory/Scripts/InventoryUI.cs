using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryContentData
{
    public Image icon;
    public Text countText;

    public InventoryContentData(Image img, Text countTxt)
    {
        icon = img;
        countText = countTxt;
    }
}

public class InventoryUI : MonoBehaviour
{
    // UI
    [SerializeField] private itemDatabase database;
    [SerializeField] private List<GameObject> contents;         // save slots
    [SerializeField] private string itemIconName = "Icon";
    [SerializeField] private string itemCountTextName = "Count";
    [SerializeField] private string inventoryWindowName;

    [SerializeField] private List<InventoryContentData> contentDatas = new List<InventoryContentData>();
    private Dictionary<string, InventoryContentData> name_ContentPairs = new Dictionary<string, InventoryContentData>();
    private GameObject inventoryWindow;

    public bool show = true;

    private void Awake()
    {
        MakeContentDatas();
        inventoryWindow = GameObject.Find(inventoryWindowName);
        show = inventoryWindow.activeSelf;
    }

    private void MakeContentDatas()
    {
        foreach(var obj in contents)
        {
            var image = obj.transform.Find(itemIconName).GetComponent<Image>();               // find Icon Image 
            var countTxt = obj.transform.Find(itemCountTextName).GetComponent<Text>();               // find Count Text

            if (!image)
                Debug.Log("Can't find icon");
            if (!countTxt)
                Debug.Log("Can't find count Text");

            contentDatas.Add(new InventoryContentData(image,countTxt));  // add new ContentData

            // hide ui first
            image.gameObject.SetActive(false);
            countTxt.gameObject.SetActive(false);
        }
    }
    

    private bool FindItemInDatabase(string itemName, out ItemData data)
    {
        data = new ItemData();
        foreach (var i in database.itemDatas)
        {
            if (i.name == itemName)
            {
                data = i;
                return true;
            }
        }
        return false;
    }

    public void SetUI(string itemName, int count)
    {
        if (FindItemInDatabase(itemName, out var data))
        {
            if (name_ContentPairs.ContainsKey(itemName))
            {
                name_ContentPairs[itemName].countText.text = count.ToString();       // set only item's count
            }
            else
            {
                Debug.Log("Set new item UI");
                var content = contentDatas[name_ContentPairs.Count];
                content.icon.sprite = data.sprite;    // set new item sprite
                content.icon.gameObject.SetActive(true);
                content.countText.text = count.ToString();       // set new item's count
                content.countText.gameObject.SetActive(true);
                name_ContentPairs.Add(itemName, content);       // add new item content
            }
        }
    }

    public void RemoveUI(string itemName)
    {
        name_ContentPairs[itemName].countText.gameObject.SetActive(false);      
        name_ContentPairs[itemName].icon.gameObject.SetActive(false);       
        name_ContentPairs.Remove(itemName);        
    }

    public void ShowUI(bool active)
    {
        inventoryWindow.SetActive(active);
        show = active;
    }
}
