using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    static private Dictionary<string, int> name_CountPairs = new Dictionary<string, int>();
    static private InventoryUI inventoryUI;

    private void Start()
    {
        inventoryUI = GetComponent<InventoryUI>();
    }

    public static void PushItem(string itemName)
    {
        Debug.Log("Push Item : " + itemName);
        if (name_CountPairs.ContainsKey(itemName))
        {
            name_CountPairs[itemName]++;
        }
        else
        {
            Debug.Log("New Item : " + itemName);
            name_CountPairs.Add(itemName, 1);
        }

        inventoryUI.SetUI(itemName, name_CountPairs[itemName]);     // ui count++
    }

    public static void PopItem(string itemName, int count)
    {
        if (name_CountPairs.ContainsKey(itemName))
        {
            name_CountPairs[itemName] -= count;
            if (name_CountPairs[itemName] <= 0)
            {
                name_CountPairs.Remove(itemName);
                // remove sprite
                // set item count UI to 0
                inventoryUI.RemoveUI(itemName);
            }
        }
    }

    public static void ShowInventoryUI(bool isShow)
    {
        inventoryUI.ShowUI(isShow);
    }

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.I))
    //    {
    //        ShowInventoryUI(!inventoryUI.show);
    //    }
    //}
}
