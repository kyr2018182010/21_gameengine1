using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class NPCAI : MonoBehaviour
{
    // animation
    [Header("====Animation====")]
    [SerializeField] RuntimeAnimatorController controller;
    [SerializeField] Avatar avatar;
    private Animator _animator;
    readonly private int _hashIdleNum = Animator.StringToHash("NPC Idle Num");
    readonly private int _hashWalk = Animator.StringToHash("NPC Walk");
    readonly private int _hashWalkNum = Animator.StringToHash("NPC Walk Num");
    readonly private int _hashRun = Animator.StringToHash("NPC Run");
    readonly private int _hashTalk = Animator.StringToHash("NPC Talk");
    readonly private int _hashTalkNum = Animator.StringToHash("NPC Talk Num");
    readonly private int _hashSync = Animator.StringToHash("Sync");
    readonly private int _hashAnimStart = Animator.StringToHash("Anim Start");

    // state
    [Header("====State=====")]
    [SerializeField] private NPCState[] states;
    [SerializeField] private NPCStateChangeCondition[] conditions;
    [SerializeField]private int stateIdx = 0;
    [SerializeField] bool roop = false;
    [SerializeField] private float changeTimer;
    private float restTimer = 0f;
    private bool talkNPC = false;
    private bool talking = false;
    private float animStartTimer = 0f;
    private bool animation = false;

    // move
    private NavMeshAgent _agent;
    [SerializeField] float walkingSpeed = 0f;
    [SerializeField] float runningSpeed = 0f;
    [SerializeField] float damping = 3f;
    private bool TileQuest = false;
    private float sync = 1f;

    // target
    [Header("====Destination====")]
    [SerializeField] Transform[] targetTransforms;
    [SerializeField] private int currTargetIdx = 0;
    [SerializeField] float range = 0.1f;

    [SerializeField] float audioOriginPitch = 1f;
    void Awake()
    {
        // 애니메이터를 설정
        _animator = gameObject.AddComponent<Animator>();
        _animator.runtimeAnimatorController = controller;
        if (avatar != null)
        {
            _animator.avatar = avatar;
        }

        // 싱크 맞추
        sync = Random.Range(0.7f, 1.1f);
        _animator.SetFloat(_hashSync, sync);
        animStartTimer = Random.Range(0f, 0.8f);

        if(conditions.Length >0 && conditions[0].condition == NPCStateChange.Time)
        {
            restTimer = conditions[0].value;
        }

        _agent = GetComponent<NavMeshAgent>();
        ChangeSpeed();         
        SetAnimation(states[stateIdx]);

        if (targetTransforms.Length != 0)
        {
            _agent.SetDestination(targetTransforms[currTargetIdx].position);
        }

        if (GetComponent<NPC>() != null)
        {
            talkNPC = true;
        }

    }

    void FixedUpdate()
    {
        animStartTimer -= Time.deltaTime;
        if (animStartTimer <= 0f && !animation)
        {
            _animator.SetTrigger(_hashAnimStart);
            animation = true;
        }

        if (talkNPC && talking)
            return;

        if (!_agent.enabled)
            return;


        if (animation)
            Move();

        if (CheckTimeCondition())
            ChangeState();

    }

    private bool CheckTimeCondition()
    {
        if (conditions.Length > stateIdx)
        {
            if (conditions[stateIdx].condition == NPCStateChange.Time)
            {
                restTimer -= Time.deltaTime;
                if (restTimer < 0f)
                {
                    restTimer = conditions[stateIdx].value;
                    return true;
                }
            }
        }
        return false;
    }

    private bool IsDestination(float range,Vector3 vec1, Vector3 vec2)
    {       
        float dist = (vec1 - vec2).magnitude;
        if ( dist <= range)
        {
            return true;
        }
        return false;
    }

    private void CheckDestination()
    {
        var vec1 = new Vector2(targetTransforms[currTargetIdx].position.x, targetTransforms[currTargetIdx].position.z);
        var vec2 = new Vector2(transform.position.x, transform.position.z);
        if (conditions.Length > 0 && conditions[stateIdx].condition == NPCStateChange.Dist)
        {
            if (IsDestination(conditions[stateIdx].value, transform.position, targetTransforms[currTargetIdx].position))
            {
                currTargetIdx = (currTargetIdx + 1) % targetTransforms.Length;     
                ChangeState();
                restTimer = conditions[stateIdx].value;
            }
        }
        else 
        {
            if (IsDestination(0.8f, vec1, vec2))
            {
                currTargetIdx = (currTargetIdx + 1) % targetTransforms.Length;      
            }
        }
        _agent.SetDestination(targetTransforms[currTargetIdx].position);
    }

    private void Turn()
    {
        Quaternion rot = Quaternion.LookRotation(_agent.desiredVelocity);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * damping);
    }

    private void Move()
    {
        if (targetTransforms.Length == 0)
            return;

        if (NPCState.Walk1 <= states[stateIdx] && states[stateIdx] <= NPCState.Run)
        {
            Turn();
        }

        CheckDestination();
    }

    private void ChangeState()
    {
        if (roop)
            stateIdx = (stateIdx + 1) % states.Length;
        else
        {
            if (stateIdx < states.Length - 1)
                stateIdx++;
        }

        SetAnimation(states[stateIdx]);

        ChangeSpeed();
    }

    private void ChangeSpeed()
    {
        if (NPCState.Idle1 <= states[stateIdx] && states[stateIdx] <= NPCState.Idle3)
            _agent.speed = 0f;
        else if (states[stateIdx] == NPCState.Walk1 || states[stateIdx] == NPCState.Walk2)
            _agent.speed = walkingSpeed * sync;
        else if (states[stateIdx] == NPCState.Run)
            _agent.speed = runningSpeed * sync;
    }

    private void SetAnimation(NPCState state)
    {
        if (NPCState.Idle1 <= state && state <= NPCState.Idle3)
        {
            _animator.SetBool(_hashWalk, false);
            _animator.SetBool(_hashRun, false);
            _animator.SetBool(_hashTalk, false);
            _animator.SetFloat(_hashIdleNum, (float)state / 2);
            
            if(_agent.enabled)
                _agent.isStopped = true;
        }
        else if (state == NPCState.Walk1 || state == NPCState.Walk2)
        {
            _animator.SetBool(_hashWalk, true);
            _animator.SetBool(_hashRun, false);
            _animator.SetBool(_hashTalk, false);

            _animator.SetFloat(_hashWalkNum, state - NPCState.Walk1);

            if (_agent.enabled)
                _agent.isStopped = false;
        }
        else if (state == NPCState.Run)
        {
            _animator.SetBool(_hashWalk, false);
            _animator.SetBool(_hashRun, true);
            _animator.SetBool(_hashTalk, false);

            if (_agent.enabled)
                _agent.isStopped = false;
        }
        else if (state == NPCState.Talk1 || state == NPCState.Talk2)
        {
            _animator.SetBool(_hashWalk, false);
            _animator.SetBool(_hashRun, false);
            _animator.SetBool(_hashTalk, true);

            _animator.SetFloat(_hashTalkNum, state - NPCState.Talk1 - 0.5f);

            if (_agent.enabled)
                _agent.isStopped = true;
        }
    }

    public void SetTalkState(bool talkState, Transform player)
    {
        if (!talkNPC)
            return;

        if (!_agent.enabled)
            return;

        if (!talkState && !talking)     // 대화 애니메이션이 잘못나오는 걸 막는다.
            return;

        if (talkState)
        {
            if (GetComponent<NPC>().isTurn)
                SetAnimation(NPCState.Talk1);

            var pos = player.position;
            pos.y = transform.position.y;
            _agent.updateRotation = false;
            _agent.SetDestination(transform.position);
            _agent.isStopped = true;
            
            if(GetComponent<NPC>().isTurn)
                transform.LookAt(pos);

            // pop items
            if(TryGetComponent<ItemPopNPC>(out var itemPopNPC))
            {
                if (!itemPopNPC.QuestStart && !itemPopNPC.QuestEnd)
                {
                    itemPopNPC.QuestStart = true;
                }
                else if (itemPopNPC.QuestStart && !itemPopNPC.QuestEnd)
                    itemPopNPC.RetrieveItems();

            }
        }
        else
        {
            _agent.updateRotation = true;
            SetAnimation(states[stateIdx]);
            if (targetTransforms.Length > 0)
            {
                _agent.isStopped = false;
                _agent.SetDestination(targetTransforms[currTargetIdx].position);
            }
        }

        talking = talkState;
    }

    public void SetFollowQuestState()
    {
        _agent.speed = runningSpeed * sync;
        _agent.updateRotation = true;
        SetAnimation(NPCState.Idle3);
        talking = false;
    }
}
