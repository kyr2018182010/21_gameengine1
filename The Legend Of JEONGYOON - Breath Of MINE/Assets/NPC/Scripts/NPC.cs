using UnityEngine;
using Subtegral.DialogueSystem.DataContainers;

public class NPC : OutlineObject
{
    private Transform face;
    public Transform Face
    {
        get => face;
    }

    public DialogueContainer dialogueContainer;
    public bool isTurn = false;

    private bool nonSelect;
    public bool NonSelect
    {
        set => nonSelect = value;
        get => nonSelect;
    }
    private void Start()
    {
        // 이후 oulineObject의 원본이 될 모델오브젝트를 찾아 변수에 저장한다.
        MakeOutlineModel();

        // 하위 오브젝트중 npc의 얼굴의 위치를 담는 transform을 받아 변수에 저장한다.
        GetFaceTransformInChildren();
    }

    private void GetFaceTransformInChildren()
    {
        var transforms = GetComponentsInChildren<Transform>();
        foreach(var tr in transforms)
        {
            if (tr.gameObject.name == "Face Position" || tr.gameObject.name == "FacePosition")
            {
                face = tr;
                return;
            }
        }
    }
}
