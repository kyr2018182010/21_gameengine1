using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallObject : MonoBehaviour
{
    private Rigidbody rigidbody;
    [Range(1,10)]public float gravity;
    private Vector3 startPos;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        startPos = transform.position;
    }

    private void OnCollisionEnter(Collision collision)
    {
        StopCoroutine(Fall());
        rigidbody.isKinematic = true;
        GameManager.GameOver();
        transform.position = startPos;
    }

    IEnumerator Fall()
    {
        while (true)
        {
            rigidbody.velocity += (gravity / rigidbody.mass) * Time.deltaTime * Vector3.down;
            yield return new WaitForFixedUpdate();
        }
    }

    public void StartFall()
    {
        StartCoroutine(Fall());
        rigidbody.isKinematic = false;
    }
}
