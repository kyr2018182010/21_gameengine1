using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Tilemaps;

public class QuestTile : MonoBehaviour
{
    private PlayerAction onPlayer;

    private bool coroutine = false;
    [SerializeField] private Vector3 autoMoveVelocity = Vector3.zero;            // only use to auto tile moving

    // auto move
    [SerializeField] private bool autoMoving = false;
    [SerializeField] private Transform[] tileTracks;
    int trackIdx = 0;
    int autoTileMoveDirection = 1;         // 1 or -1
    [Range(0f, 10f)] [SerializeField] private float autoMovingAcc = 1f;
    [Range(0f, 10f)] [SerializeField] private float waitSeconds = 2f;
    private bool moving = false;
    [SerializeField] private QuestButton moveStartTriggerButton;


    // auto tile will use transform.Translate() not rigidbody
    private void Awake()
    {
        if (tileTracks.Length == 0) Debug.LogError("You have to set auto tile tracks");
 
    }

    private void OnEnable()
    {
        if (autoMoving) StartAutoMoveing();
        moving = false;
    } 

    private void OnDisable()
    {
        if (moving)
        {
            StopCoroutine(AutoTileMoving());
        }

        if (onPlayer)
        {
            StopCoroutine(MovePlayer());
            StopCoroutine(PlayerGetDownTile());
            onPlayer.OnTile(false);
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.contacts[0].point.y <= transform.position.y)
            return;
        var player = collision.transform.GetComponentInParent<PlayerAction>();
        if (player != null)     // if collide with player, add that player to List
        {
            if (!onPlayer)
            {
                onPlayer = player;
            }
            player.OnTile(true);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.contacts[0].point.y <= transform.position.y)
            return;
        var player = collision.transform.GetComponentInParent<PlayerAction>();
        if (player != null)     // if collide with player, add that player to List
        {
            if (!onPlayer)
            {
                onPlayer = player;
            }
            player.OnTile(true);
        }
    }

    private void FixedUpdate()
    {
        if (onPlayer && !coroutine)
        {
            coroutine = true;
            StartCoroutine(MovePlayer());
            StartCoroutine(PlayerGetDownTile());
        }

        if (!onPlayer)
            coroutine = false;
    }

    private bool IsArrivePoint(Vector3 point)
    {
        if (Vector3.Distance(point, transform.position) <= 0.3f)
        {
            return true;
        }
        return false;
    }

    IEnumerator AutoTileMoving()
    {
        while (true)
        {
            var u = (tileTracks[trackIdx].position - transform.position).normalized;        // u vector
            autoMoveVelocity += (autoMovingAcc * Time.deltaTime) * u;
            transform.position += autoMoveVelocity * Time.deltaTime;
            var vec = autoMoveVelocity * Time.deltaTime;

            if (IsArrivePoint(tileTracks[trackIdx].position))   // check arrive track point
            {
                transform.position = tileTracks[trackIdx].position;
                if ((autoTileMoveDirection == 1 && trackIdx >= tileTracks.Length - 1) ||     // tile arrive last point 
                    (autoTileMoveDirection == -1 && trackIdx <= 0))                     // tile arrive first point
                {
                    autoTileMoveDirection *= -1;
                }
                trackIdx += autoTileMoveDirection;      // change next track point
                autoMoveVelocity = Vector3.zero;
                yield return new WaitForSeconds(waitSeconds);
            }

            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator MovePlayer()
    {
        while (true)
        {
            if (!onPlayer)
            {
                StopCoroutine(MovePlayer());
                break;
            }

            var vel = Vector3.zero;
            if (autoMoving) vel = autoMoveVelocity;

            onPlayer.externalVelocity = vel;
            
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator PlayerGetDownTile()
    {
        while (true)
        {
            if (!onPlayer)
            {
                StopCoroutine(PlayerGetDownTile());
                break;
            }

            // create temp array


                if (!CheckOnTile(onPlayer.transform.position, out var hitPoint))       // if player get down tile
                {
                    onPlayer.OnTile(false);
                    onPlayer = null;
                }
            

            yield return new WaitForSeconds(0.1f);

        }
    }

    private bool CheckOnTile(Vector3 feetPos, out Vector3 point)
    {
        if (Physics.Raycast(feetPos + Vector3.up * 0.1f, Vector3.down, out var hit, 5f))
        {
            if (hit.transform.TryGetComponent<QuestTile>(out var tile))
            {
                point = hit.point;
                return true;
            }
        }
        point = new Vector3(-100000, -10000, -10000);
        return false;
    }

    public void AllGetDownTile()
    {
        if (onPlayer)
        {

                onPlayer.OnTile(false);

        }

    }

    public void StartAutoMoveing()
    {
        if (autoMoving && !moving) StartCoroutine(AutoTileMoving());
        moving = true;
    }
}
