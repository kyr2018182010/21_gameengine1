using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestButton : QuestObj
{
    private Vector3 yOffset = new Vector3(0, 0.005f, 0);
    private float pushAmount = 0.5f;
    private float changedY;

    [SerializeField]
    private List<QuestDoor> questDoors = new List<QuestDoor>();
    [SerializeField] private FallObject fallObject;

    private AudioSource audioSource;
    [SerializeField]private AudioClip audioClip;
    private void Start()
    {
        changedY = transform.position.y - pushAmount;
        audioSource = GetComponent<AudioSource>();
    }


    private void OnCollisionEnter(Collision collision)
    {
        // 플레이어일 때
        var player = collision.transform.GetComponentInParent<PlayerAction>();
        if (player)
        {
            Push();
        }
    }

    private void Push()
    {
        if(!gameObject.GetComponent<Rigidbody>().isKinematic)
            StartCoroutine("PushButton");
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        audioSource.PlayOneShot(audioClip);
        isActive = true;
        for (int i = 0; i < questDoors.Count; ++i)
            questDoors[i].IsButtonOk();
        if (fallObject)
            fallObject.StartFall();
    }

    private IEnumerator PushButton()
    {
        while (this.transform.position.y > changedY)
        {
            this.transform.position -= yOffset;
            yield return new WaitForSeconds(0.001f);
        }
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
    }
}
