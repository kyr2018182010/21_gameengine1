using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.UI;

public class QuestElectric : QuestObj
{
    [SerializeField]
    private List<QuestDoor> questDoors = new List<QuestDoor>();
    [SerializeField] GameObject showedObjects;
    [SerializeField]
    private float time = 240f;
    [SerializeField]
    private VisualEffect effect;
    [SerializeField]
    private float turnOnTime = 3f;
    [SerializeField]
    private GameObject lineEffect;

    [SerializeField] private GameObject timer;
    private Text timerText;

    [SerializeField] private AudioClip effectSound;
    private AudioSource audioSource;

    private float lessTurnOnTime;
    private float timeOffset;
    private float lessTime = 240f;
    private float effectSize;
    private float defaultEffectSize;


    private void Start()
    {
        defaultEffectSize = effect.GetFloat("time");
        lessTime = time;       
        lessTurnOnTime = turnOnTime;
        if(timer)
            timerText = timer.GetComponentInChildren<Text>();

        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.root.TryGetComponent<PlayerAction>(out var player))
        {
            TurnOnTimer();
        }
    }

    private void TurnOnTimer()
    {
        float t = Time.deltaTime;
        lessTurnOnTime -= t;
        //Debug.Log(lessTurnOnTime);

        effectSize = effect.GetFloat("time");
        timeOffset = effectSize / (lessTurnOnTime / t);
        effect.SetFloat("time", effectSize -= timeOffset);

        if (lessTurnOnTime < 0)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            audioSource.PlayOneShot(effectSound);
            lineEffect.SetActive(true);
            StartCoroutine("Timer");
            isActive = true;
            for (int i = 0; i < questDoors.Count; ++i)
                questDoors[i].IsButtonOk();
            timer.SetActive(true);
            StartCoroutine(ShowLessTime());
            if (showedObjects)
            {
                showedObjects.SetActive(true);
            }
        }
    }

    private IEnumerator Timer()
    {
        while (lessTime > 0)
        {
            float t = Time.deltaTime;
            lessTime -= t;
            yield return new WaitForSeconds(t);
        }
        timer.gameObject.SetActive(false);
        StopCoroutine(ShowLessTime());
        if (showedObjects)
        {
            showedObjects.SetActive(false);
        }
        effectSize = defaultEffectSize;
        lessTime = time;
        lessTurnOnTime = turnOnTime;
        isActive = false;
        for (int i = 0; i < questDoors.Count; ++i)
            questDoors[i].CloseDoor();

        effect.SetFloat("time", effectSize);
        gameObject.GetComponent<BoxCollider>().enabled = true;
        lineEffect.SetActive(false);

    }

    IEnumerator ShowLessTime()
    {
        while (true)
        {
            if (!timer.activeSelf)
            {
                StopCoroutine(ShowLessTime());
                break;
            }
            timerText.text = ((int)(lessTime)).ToString();
            yield return new WaitForEndOfFrame();
        }
    }
}

