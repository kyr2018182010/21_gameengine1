
using UnityEngine;

public class CameraMoving : MonoBehaviour
{
    [Header("======Camera Controller Value=======")]
    [SerializeField]private float mouseXSensitivity = 100f;
    [SerializeField]private float mouseYSensiticity = 10f;
    [SerializeField]private float thirdMinY = 0f;
    [SerializeField]private float thirdMaxY = 5f;
    [SerializeField]private float firstMinY = 0.3f;
    [SerializeField] private float firstMaxY = 3f;
    
    [Header("======Camera Anim & Follow=========")]
    [SerializeField] private Transform thirdCameraFollow;

    [SerializeField] private Transform firstCameraLookAt;


    [Header("========Target=====================")]
    [SerializeField] private PlayerAction player;
    [SerializeField] private SkillManager _skillManager;

    private float _defualt3rdHeight;
    private float _defualt1stHeight;
    private float _thirdDeltaY = 0f;
    private float _firstDeltaY = 0f;

    static public float MouseXSensitivity;

    private void Start()
    {
        if (_skillManager == null)
            Debug.LogError("Camera Moving : Skill Manager is NULL");
        if (player == null)
            Debug.LogError("Camera Moving : player is NULL");
        else
        {
            firstCameraLookAt = GameObject.Find("1st Camera Aim").transform;
            thirdCameraFollow = GameObject.Find("3rd Camera Follow").transform;
        }
        if (firstCameraLookAt == null)
            Debug.LogError("Camera Moving : 1st Camera Aim is NULL");
        if (thirdCameraFollow == null)
            Debug.LogError("Camera Moving : 3rd Camera Aim is NULL");

        _defualt1stHeight = firstCameraLookAt.position.y - player.transform.position.y;
        _defualt3rdHeight = thirdCameraFollow.position.y - player.transform.position.y;

        MouseXSensitivity = mouseXSensitivity;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player.State == PlayerState.Talk)
            return;

        // GameInputManager에서 set된 값을 가져온다.
        float rotX, rotY;
        rotX = GameInputManager.inputData.CameraData.x;
        rotY = GameInputManager.inputData.CameraData.y;
       
        // 플레이어를 x축 방향으로 회전시킨다.
        player.transform.Rotate(Vector3.up * (rotX * Time.deltaTime * mouseXSensitivity));

        // 카메라의 이동량을 계산해 이동시킨다.
        float deltaY = rotY * Time.deltaTime * mouseYSensiticity;
        if (_skillManager.isKeyHolding)
        {
            // 1st
            // min / max ???
            float checkY = (firstCameraLookAt.position.y + deltaY) - player.transform.position.y;
            if (checkY < firstMinY)
            {
                rotY = 0f;
            }
            else if (checkY > firstMaxY)
            {
                rotY = 0f;
            }
            firstCameraLookAt.Translate(Vector3.up * rotY * Time.deltaTime * mouseYSensiticity);

            // 3rd ???
            if (thirdCameraFollow.position.y - transform.position.y != _defualt3rdHeight)
            {
                thirdCameraFollow.SetPositionAndRotation(new Vector3(thirdCameraFollow.position.x, player.transform.position.y + _defualt3rdHeight, thirdCameraFollow.position.z), transform.rotation);
            }

            if (_thirdDeltaY != 0f)
                _thirdDeltaY = 0f;
        }
        else
        {
            // 3rd          
            // ?浹 ???
            if (Physics.Raycast(thirdCameraFollow.position - Vector3.up * deltaY, Vector3.down, out var hit, thirdMaxY))
            {
                 if(thirdCameraFollow.position.y - deltaY - hit.transform.position.y <= 0.5f)
                {
                    Debug.Log("Collision");
                    rotY = 0f;
                }
            }

            // min / max ???
            float checkY = (thirdCameraFollow.position.y - deltaY) - player.transform.position.y;
            if (checkY < thirdMinY)
            {
                rotY = 0f;
            }
            else if (checkY  > thirdMaxY)
            {
                rotY = 0f;
            }


            thirdCameraFollow.Translate(Vector3.up * (-rotY * Time.deltaTime * mouseYSensiticity));

            if (firstCameraLookAt.position.y - player.transform.position.y != _defualt1stHeight)
            {
                firstCameraLookAt.SetPositionAndRotation(new Vector3(firstCameraLookAt.position.x, player.transform.position.y + _defualt1stHeight, firstCameraLookAt.position.z), player.transform.rotation);
            }
            if (_firstDeltaY != 0f)
                _firstDeltaY = 0f;
        }
    }

}
