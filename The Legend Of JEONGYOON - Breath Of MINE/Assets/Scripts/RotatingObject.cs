using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingObject : MonoBehaviour
{
    private float rotdata;
    [SerializeField] private float rotatingSpeed = 5f;

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(Vector3.up * rotdata * Time.deltaTime);
        rotdata += rotatingSpeed;
    }
}
