using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlinePresettings: MonoBehaviour
{
    public Material outlineMaterial;
    public Material npcOutlineMaterial;
    public float outlineScale;
    public float npcOutlineScale;
    public Color outlineColor;

    static public Material _outlineMaterial;
    static public Material _npcOutlineMaterial;

    void Awake()
    {
        //메터리얼의 프로퍼티를 설정한다.
        outlineMaterial.SetColor("_OutlineColor", outlineColor);
        outlineMaterial.SetFloat("_Scale", outlineScale);
        npcOutlineMaterial.SetColor("_OutlineColor", outlineColor);
        npcOutlineMaterial.SetFloat("_Scale", npcOutlineScale);

        _npcOutlineMaterial = npcOutlineMaterial;
        _outlineMaterial = outlineMaterial;
    }


}
