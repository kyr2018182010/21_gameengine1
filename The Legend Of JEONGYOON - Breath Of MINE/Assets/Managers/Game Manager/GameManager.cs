using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameState
{
    MainGame,
    Pause,
    GameEnd,
    GameOver
}

public class GameManager : MonoBehaviour
{
    static GameObject panel;
    static Text endText;
    static Text announceText;
    static private PlayerAction player;

    static public GameState currState;

    [SerializeField] private Texture2D cursorTexture2D;
    static private Vector3 spawnPoint;

    private void Awake()
    {
        panel = GameObject.Find("End Panel");
        endText = GameObject.Find("End Text").GetComponent<Text>();
        announceText = GameObject.Find("Announce").GetComponent<Text>();
        panel.SetActive(false);
        player = FindObjectOfType<PlayerAction>();
        spawnPoint = player.transform.position;
        currState = GameState.MainGame;
        Cursor.SetCursor(cursorTexture2D, new Vector2(0,0), CursorMode.Auto);
        ShowCursor(false);
    }

    private void Update()
    {
        if (Input.GetButtonDown("LeftClick"))
        {
            if(currState == GameState.GameOver)
            {
                panel.SetActive(false);
                ShowCursor(false);
                currState = GameState.MainGame;
            }else if(currState == GameState.GameEnd)
            {
                ExitGame();
            }
        }
    }
    public static void GameEnd()
    {
        panel.SetActive(true);
        endText.text = "GAME END";
        announceText.text = "Click to Exit!";
        endText.color = Color.white;
        Inventory.ShowInventoryUI(false);
        ShowCursor(true);
        currState = GameState.GameEnd;
    }

    public static void GameOver()
    {
        panel.SetActive(true);
        endText.text = "GAME OVER";
        announceText.text = "Click to Restart!";
        endText.color = Color.red;
        Inventory.ShowInventoryUI(false);
        player.SetNewTransform(spawnPoint);
        ShowCursor(true);
        currState = GameState.GameOver;
    }

    public static void ShowCursor(bool show)
    {
        if (show)
        {
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        Cursor.visible = show;
    }

    static public void SetSpawnPoint(Vector3 pos)
    {
        spawnPoint = pos;
    }

    private void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit(); // 어플리케이션 종료
#endif
    }
}
