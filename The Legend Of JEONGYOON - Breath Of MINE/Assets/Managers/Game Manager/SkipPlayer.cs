using Subtegral.DialogueSystem.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipPlayer : MonoBehaviour
{
    private PlayerAction Player;
    [SerializeField] private Transform[] transforms;
    [SerializeField] private int currTransformIdx = 0;
    [SerializeField] private KeyCode NextKey;
    [SerializeField] private KeyCode PrevKey;
    [SerializeField] private QuestContainer[] questContainers;
    [SerializeField] private int currQuestIdx = 0;
    [SerializeField] private KeyCode NextQuestKey;
    [SerializeField] private KeyCode PrevQuestKey;
    // Update is called once per frame

    private QuestParser questParser;
    private void Start()
    {
        questParser = Camera.main.transform.GetComponent<QuestParser>();
        Player = FindObjectOfType<PlayerAction>();
    }
    void Update()
    {
        if (Input.GetKeyDown(NextKey))
        {
            if (currTransformIdx < transforms.Length - 1)
            {
                Player.SetNewTransform(transforms[++currTransformIdx].position);
            }
        }

        if (Input.GetKeyDown(PrevKey))
        {
            if (currTransformIdx > 0)
            {
                Player.SetNewTransform(transforms[--currTransformIdx].position);
            }
        }

        if (Input.GetKeyDown(NextQuestKey))
        {
            if (currQuestIdx < transforms.Length - 1)
            {
                questParser.quest = questContainers[++currQuestIdx];
                questParser.ResetQuest();
            }
        }

        if (Input.GetKeyDown(PrevQuestKey))
        {
            if (currQuestIdx > 0)
            {
                questParser.quest = questContainers[--currQuestIdx];
                questParser.ResetQuest();
            }
        }
    }
}
