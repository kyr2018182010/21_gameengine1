using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnOffBGM : MonoBehaviour
{
    [SerializeField] BGMType bgmType;

    private void OnEnable()
    {
        AudioManager.ChangeBGM(bgmType);
    }

    private void OnDisable()
    {
        AudioManager.ChangeBGM2Common();
    }
}
