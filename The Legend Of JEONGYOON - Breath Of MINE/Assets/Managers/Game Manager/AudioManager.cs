using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BGMType
{
    commonBGM = 0,
    Timer,
}

[System.Serializable]
public struct AudioData
{
    public AudioClip clip;
    [Range(0,3)] public float pitch;
    [Range(0,1)] public float volume;
}

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioData[] BGMs;
    static private AudioSource mainAudioSource;

    static private AudioData[] bgms;
    static private BGMType currBGMType;

    private void Start()
    {
        bgms = BGMs;
        if (!mainAudioSource)
            mainAudioSource = Camera.main.GetComponent<AudioSource>();
        currBGMType = BGMType.commonBGM;
    }

    static public void ChangeBGM(BGMType type)
    {
        if (currBGMType != type)
        {
            mainAudioSource.clip = bgms[(int)type].clip;
            mainAudioSource.pitch = bgms[(int)type].pitch;
            mainAudioSource.volume = bgms[(int)type].volume;
            currBGMType = type;
            mainAudioSource.Play();
        }
    }

    static public void ChangeBGM2Common()
    {
        if (currBGMType != BGMType.commonBGM)
        {
            mainAudioSource.clip = bgms[(int)BGMType.commonBGM].clip;
            mainAudioSource.pitch = bgms[(int)BGMType.commonBGM].pitch;
            mainAudioSource.volume = bgms[(int)BGMType.commonBGM].volume;
            currBGMType = BGMType.commonBGM;
            mainAudioSource.Play();
        }
    }

    static public void StopBGMSound()
    {
        mainAudioSource.Stop();
    }
}
