using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEndCollection : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (GameManager.currState != GameState.MainGame) return;

        if (Input.GetButtonDown("LeftClick"))
        {
            GameManager.GameEnd();
        }
    }
}
