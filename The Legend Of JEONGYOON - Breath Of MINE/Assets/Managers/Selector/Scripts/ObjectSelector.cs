using UnityEngine;

public class ObjectSelector : MonoBehaviour
{
    [SerializeField]private SkillManager _skillManager;


    [HideInInspector] public PlayerAction player;

    private IHilightSelectionResponse _highlihgtSelectionResponse;

    private Skill2Object _selection;
    private Vector3 hitPoint;
    private bool creatable;

    [SerializeField] float range;
    [SerializeField] string CreationAreaTag = "Creation Area";

    [SerializeField]  private Transform eyePosition;

    [SerializeField]  private Transform firstCameraLookAtPostion;


    private void Start()
    {
        if (eyePosition == null)
            Debug.LogError("Object Selector EyePositon : " + eyePosition);
        if (firstCameraLookAtPostion == null)
            Debug.LogError("Object Selector firstCameraLookAtPostion : " + firstCameraLookAtPostion);
        if (_skillManager == null)
            Debug.LogError("Object Selector SkillManager : " + _skillManager);

        _highlihgtSelectionResponse = GetComponent<IHilightSelectionResponse>();
    }

    private void FixedUpdate()
    {
        if (_selection != null)
        {
            _highlihgtSelectionResponse.OnDeselect(_selection.transform);
            _selection = null;

        }

        if (_skillManager.isKeyHolding)
        {
            Check();
        }

        if (_selection != null)
        {

            if (_selection.GetComponent<Skill2Object>() != null)
            {
                // if Skill2 Object is selected 
                _skillManager._selection = _selection;
                _highlihgtSelectionResponse.OnSelect(_selection.transform);

            }
        }

        _skillManager.CreationPoint = hitPoint;
        _skillManager.Creatable = creatable;

    }

    private void Check()
    {
        Vector3 look = firstCameraLookAtPostion.position - eyePosition.position;
        Debug.DrawRay(eyePosition.position, look, Color.blue, range);
        if (Physics.Raycast(eyePosition.position, look, out var hit, range))
        {
            var skillObj = hit.transform.GetComponent<Skill2Object>();
            if (skillObj != null)
            {
                _selection = skillObj;
            }
            if (hit.transform.CompareTag(CreationAreaTag))
            {
                hitPoint = hit.point;
                creatable = true;
            }
            else
            {
                hitPoint = new Vector3(-100000, -100000, -100000);
                creatable = false;
            }
        }
    }
}
