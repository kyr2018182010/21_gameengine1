using Subtegral.DialogueSystem.Runtime;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Subtegral.DialogueSystem.DataContainers;

public class NPCSelector : MonoBehaviour
{
    private Transform _playerTransform;
    private Transform[] _npcTransforms;

    [SerializeField] private float range;
    [SerializeField] private float fov;

    private Transform _selection = null;

    // UI
    private IHilightSelectionResponse _hilightSelectionResponse;


    // Quest Parser
    private QuestParser questParser;

    [SerializeField]private PlayerAction playerAction;
    [SerializeField]private CinemachineVirtualCamera talkVirtualCam;

    private void Start() 
    {
        // Check Error
        if (playerAction == null)
            Debug.LogError("NPC Selector Player: " + playerAction);
        if (talkVirtualCam == null)
            Debug.LogError("NPC Selector talkCam: " + talkVirtualCam);

        NPC[] npcs = FindObjectsOfType<NPC>();
        _npcTransforms = new Transform[npcs.Length];
        for(int i = 0; i < npcs.Length; ++i)
        {
            _npcTransforms[i] = npcs[i].GetComponent<Transform>();
        }
        _hilightSelectionResponse = GetComponent<IHilightSelectionResponse>();

        questParser = FindObjectOfType<QuestParser>();
        _playerTransform = playerAction.transform;
    }

    private void FixedUpdate()
    {
        if (playerAction.State == PlayerState.Talk)
            return;

        // UI를 지운다.
        if(_selection != null)
        {
            _hilightSelectionResponse.OnDeselect(_selection);
            _selection.GetComponent<NPCAI>().SetTalkState(false,null);
        }

        // 시야내에 존재하는지 검사한다.
        _selection = null;
        Check();
        
        // UI를 띄운다.
        if (_selection != null) {
            _hilightSelectionResponse.OnSelect(_selection);
        }
    }

    private void Check()
    {
        // 모든 NPC들의 거리를 계산해 range에 포함되는 NPC만 남긴다.
        List<int> inRangeNpcIdxs = new List<int>();
        for(var  i = 0; i < _npcTransforms.Length; ++i)
        {
            if (!_npcTransforms[i].GetComponent<NPC>().NonSelect)
            {
                var dist = (_npcTransforms[i].transform.position - _playerTransform.position).magnitude;
                if (dist <= range)
                {
                    inRangeNpcIdxs.Add(i);
                }
            }
        }

        // fov 값을 벗어난 뱡향에 있는경우 제외시킨다.
        List<int> inFov = new List<int>();
        for (int i = 0; i < inRangeNpcIdxs.Count; ++i)
        {
            var dir = (_npcTransforms[inRangeNpcIdxs[i]].transform.position - _playerTransform.position).normalized;
            if (Vector3.Angle(_playerTransform.forward, dir) <= fov * 0.5)
            {
                inFov.Add(inRangeNpcIdxs[i]);
            }
        }

        if (inFov.Count > 0)
        {
            var selectedNPC = inFov[0];            // 선택되는 npc가 한개인 경우
            // 선택되는 npc가 여러개인 경우에 대해 처리한다. -> 최소 거리에 있는 npc를 선택하도록 한다.
            if (inFov.Count > 1)
            {
                var minDist = (_npcTransforms[inFov[0]].transform.position - _playerTransform.position).magnitude;
                for (var i = 1; i < inFov.Count; ++i)
                {
                    var dist = (_npcTransforms[inFov[i]].transform.position - _playerTransform.position).magnitude;
                    if (minDist > dist)
                    {
                        selectedNPC = inFov[i];
                        minDist = dist;
                    }
                }
            }
            _selection = _npcTransforms[selectedNPC];
        }
    }

    public void SelectionButton()
    {
        if (playerAction.State != PlayerState.Talk && _selection != null)
        {
            if (questParser.IsQuestGiver(_selection.gameObject.name, false))
            {
                object[] content = new object[] { _selection.gameObject.name };

                playerAction.SetTalkState(true, _selection);
                _selection.GetComponent<NPCAI>().SetTalkState(true, playerAction.transform);
                talkVirtualCam.LookAt = _selection.GetComponent<NPC>().Face;
                _hilightSelectionResponse.OnDeselect(_selection);
            }

            else if (questParser.CheckTalkPartner(_selection.gameObject.name, false))
            {
                object[] content = new object[] { _selection.gameObject.name };

                playerAction.SetTalkState(true, _selection);
                _selection.GetComponent<NPCAI>().SetTalkState(true, playerAction.transform);
                talkVirtualCam.LookAt = _selection.GetComponent<NPC>().Face;
                _hilightSelectionResponse.OnDeselect(_selection);
            }
            else if (_selection.GetComponent<NPC>().dialogueContainer != null)
            {
                playerAction.SetTalkState(true, _selection);
                _selection.GetComponent<NPCAI>().SetTalkState(true, playerAction.transform);
                talkVirtualCam.LookAt = _selection.GetComponent<NPC>().Face;
                _hilightSelectionResponse.OnDeselect(_selection);

                DialogueParser dialogueParser = Camera.main.GetComponent<DialogueParser>();
                dialogueParser.dialogue = _selection.GetComponent<NPC>().dialogueContainer;
                dialogueParser.StartTalk("talk default", false);
            }
        }
    }
}
